#!/bin/bash

echo "Please, enter a number of month: "
read number
case $number in
1) 
echo "January - 31 days" ;;
2)
echo "February - 28 days in a common year and 29 days in leap years" ;;
3)
echo "March - 31 days" ;;
4)
echo "April - 30 days" ;;
5)
echo "May - 31 days" ;;
6)
echo "June - 30 days" ;;
7)
echo "July - 31 days" ;;
8)
echo "August - 31 days" ;;
9)
echo "September - 30 days" ;;
10)
echo "October - 31 days" ;;
11)
echo "November - 30 days" ;;
12)
echo "December - 31 days" ;;
*)
echo "Incorrect number of month" ;;
esac